# frozen_string_literal: true

module Chatops
  module Commands
    # Namespace class
    class Namespace
      include Command

      usage "#{command_name} [namespace id]"
      description 'Look up namespace information.'

      def perform
        namespace_id = arguments[0]
        return 'You must supply a namespace ID to look up.' unless namespace_id

        get_namespace(namespace_id)
      end

      def get_namespace(namespace_id)
        namespace_info = Gitlab::Client
          .new(token: gitlab_token)
          .find_namespace(namespace_id)

        submit_namespace_details(namespace_info)
      end

      def submit_namespace_details(namespace)
        Slack::Message
          .new(token: slack_token, channel: channel)
          .send(
            attachments: [
              {
                fields: [
                  {
                    title: 'ID',
                    value: namespace.id,
                    short: true
                  },
                  {
                    title: 'Name',
                    value: namespace.name,
                    short: true
                  },
                  {
                    title: 'Kind',
                    value: namespace.kind,
                    short: true
                  },
                  {
                    title: 'Path',
                    value: namespace.path,
                    short: true
                  },
                  {
                    title: 'Billable members',
                    value: namespace.billable_members_count,
                    short: true
                  },
                  {
                    title: 'Plan',
                    value: namespace.plan,
                    short: true
                  }
                ]
              }
            ]
          )
      end
    end
  end
end
